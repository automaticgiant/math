import java.io.Serializable;




// TODO: Auto-generated Javadoc
/**
 * The Class DiffyQSystem.
 */
public class DiffyQSystem implements Serializable {
	
	/** The name. */
	private String name;
	
	/** The equations. */
	private int equations;	// scalar, n
	
	/** The p. */
	private String[][] p;	// [n x n]
	
	/** The ivp. */
	private IVP IVP;		// [n-1 x 1],t
	
	/** The g. */
	private String[] g;		// [n-1 x 1]
	
	/**
	 * Gets the prints the width.
	 *
	 * @return the prints the width
	 */
	private int getPrintWidth() {
		int maxWidth=0;
		for (int r = 0; r < getEquations(); r++) {
			int currentWidth=1+getEquations();	//should be spaces for border and inter-column spacing
			for (int c = 0; c < getEquations(); c++) currentWidth+=getP()[r][c].length();
			if (currentWidth>maxWidth) maxWidth=currentWidth;
		}
		return maxWidth;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder string = new StringBuilder("System of size " + getEquations() + ", called " + name + "\ny'={");
		for (int r = 0; r < getEquations(); r++) {
			string.append("{");
			for (int c = 0; c < getEquations(); c++) {
				string.append(getP()[r][c]);
				if (c<getEquations()-1) string.append(",");
			}
			
			if (r==getEquations()-1) string.append("}}");
			else string.append("},");
		}
		string.append("*y+g | g=<");
		for (int r = 0; r < getEquations(); r++) {
			string.append(getG()[r]);
			if (r<getEquations()-1) string.append(",");
		}
		string.append(">, y(" + getIVP().getT() + ")=<");
		for (int r = 0; r < getEquations(); r++) {
			string.append(getIVP().getValues()[r]);
			if (r<getEquations()-1) string.append(",");
		}
		string.append(">\n");
		
		
		return string.toString();
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Instantiates a new diffy q system.
	 *
	 * @param size the size
	 * @throws Exception the exception
	 */
	public DiffyQSystem(int size) throws Exception {
		if (size > 0) this.equations = size;
		else throw new Exception("System size must be >0");
		this.IVP = new IVP();
	}

	/**
	 * Gets the p.
	 *
	 * @return the p
	 */
	public String[][] getP() {
		return p;
	}

	/**
	 * Sets the p.
	 *
	 * @param p the new p
	 * @throws Exception the exception
	 */
	public void setP(String[][] p) throws Exception {
		if ( p.length == getEquations() && setPHelper(p) ) this.p = p;
			else throw new Exception("Matrix dimensions do not match system dimensions.");
	}
	
	/**
	 * Sets the p helper.
	 *
	 * @param p the p
	 * @return true, if successful
	 */
	private boolean setPHelper(String[][] p) {
		for ( int i = 0; i < p.length; i++ ) if ( p[i].length !=getEquations() ) return false;
		return true;
	}

	/**
	 * Gets the ivp.
	 *
	 * @return the ivp
	 */
	public IVP getIVP() {
		return IVP;
	}

	/**
	 * Sets the ivp.
	 *
	 * @param IVP the new ivp
	 * @throws Exception the exception
	 */
	public void setIVP(IVP IVP) throws Exception {
		if ( IVP.getValues().length == getEquations() ) this.IVP = IVP;
		else throw new Exception("Vector length does not match system dimension.");
	}

	/**
	 * Gets the equations.
	 *
	 * @return the equations
	 */
	public int getEquations() {
		return equations;
	}

	/**
	 * Gets the g.
	 *
	 * @return the g
	 */
	public String[] getG() {
		return g;
	}

	/**
	 * Sets the g.
	 *
	 * @param g the new g
	 * @throws Exception the exception
	 */
	public void setG(String[] g) throws Exception {
		if ( g.length == getEquations() ) this.g = g;
		else throw new Exception("Vector length does not match system dimension.");
	}

	/**
	 * The Class IVP.
	 */
	public class IVP implements Serializable {
		
		/** The t. */
		private double t;			// independent variable
		
		/** The values. */
		private double[] values;	// dependent variables @ t, y(t), [n x 1]
		
		/**
		 * Gets the values.
		 *
		 * @return the values
		 */
		public double[] getValues() {
			return values;
		}
		
		/**
		 * Sets the values.
		 *
		 * @param values the new values
		 * @throws Exception the exception
		 */
		public void setValues(double[] values) throws Exception {
			if ( values.length == getEquations() ) this.values = values;
			else throw new Exception("Vector length does not match system dimension.");
		}
		
		/**
		 * Gets the t.
		 *
		 * @return the t
		 */
		public double getT() {
			return t;
		}
		
		/**
		 * Sets the t.
		 *
		 * @param t the new t
		 */
		public void setT(double t) {
			this.t = t;
		}
	}
}
