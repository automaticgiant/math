import java.io.*;
import java.util.*;

import org.softwaremonkey.www.MathEval;

// TODO: Auto-generated Javadoc
/**
 * The Class EulerSystemApproximation.
 */
public class EulerSystemApproximation {
	
	/** The stdin. */
	static Scanner stdin = new Scanner(System.in);
	
	/** The systems. */
	static Map<String, DiffyQSystem> systems = new TreeMap<>(); // storage for systems
	
	/** The settings. */
	static Object[] settings = { 10, 0.1, 1 }; // steps, delta, nth
	
	/** The Constant roundPlaces. */
	static final int roundPlaces = 4; //little hack to fix inexact floating point decimal business - needs tweaked if you want delta smaller than .0001

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public static void main(String args[]) throws Exception {

		ObjectInputStream objectIn = null;

		// here are the quiz systems. you get these for free.
		DiffyQSystem systemQuizA = new DiffyQSystem(2);
		systemQuizA.setName("Quiz 4.9 1a");
		systemQuizA.setP(new String[][] { { "1/t", "sin(t)" }, { "1-t", "1" } });
		systemQuizA.setG(new String[] { "0", "t^2" });
		systemQuizA.getIVP().setT(1);
		systemQuizA.getIVP().setValues(new double[] { 0, 0 });
		systems.put(systemQuizA.getName(), systemQuizA);
		systemQuizA = null;
		DiffyQSystem systemQuizB = new DiffyQSystem(2);
		systemQuizB.setName("Quiz 4.9 1b");
		systemQuizB.setP(new String[][] { { "0", "1" }, { "-t^2", "-1" } });
		systemQuizB.setG(new String[] { "0", "2" });
		systemQuizB.getIVP().setT(0);
		systemQuizB.getIVP().setValues(new double[] { 1, 1 });
		systems.put(systemQuizB.getName(), systemQuizB);
		systemQuizB = null;

		try {
			objectIn = new ObjectInputStream(new FileInputStream("systems"));
			try {
				systems.putAll((Map<? extends String, ? extends DiffyQSystem>) objectIn
						.readObject());
				settings = (Object[]) objectIn.readObject();
			} catch (Exception e) {
				System.out.println("That's no good. Parse error: " + e);
			}
			objectIn.close();
		} catch (Exception e) {
			System.out.println("That's no good. File access error: " + e);
		}

		String input = "";
		// menu loop
		while (!input.equals("done")) {
			System.out.println("Defined systems are:");
			for (String each : systems.keySet())
				System.out.println(systems.get(each).getName());
			System.out
					.println("Type the name of a system to use, or new, remove, clear, csv or done:");
			input = stdin.nextLine();
			switch (input.toLowerCase()) {
			case "new":
				newSystem();
				break;
			case "remove":
				System.out.print("System: ");
				try {
					systems.remove(systems.get(stdin.nextLine()));
				} catch (Exception e) {
					System.out.println("That didn't work: " + e);
				}
				break;
			case "clear":
				systems.clear();
				settings = new Object[] { 10, 0.1, 1 }; // steps, delta, nth
				break;
			case "csv":
				csv();
				break;
			case "":
				break;
			case "done":
				break;
			default:
				if (systems.containsKey(input))
					approxWizard(systems.get(input));
				else System.out.println("That didn't work.");
				
			}
		}

		ObjectOutputStream objectOut = new ObjectOutputStream(
				new FileOutputStream("systems"));
		objectOut.writeObject(systems);
		objectOut.writeObject(settings);
		objectOut.close();

	}

	/**
	 * Csv.
	 */
	private static void csv() {
		System.out
				.println("This will export results to approx.csv, appending sets to an existing file.");
		System.out
				.println("Specify the system and we'll use the last run's parameters.");
		String name = stdin.nextLine();
		double approximation[][];
		DiffyQSystem system;
		PrintWriter out;
		if (systems.containsKey(name)) {
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter("approx.csv", true)));
				int steps = (int) settings[0];
				double delta = (double) settings[1];
				int nth = (int) settings[2];
				system = systems.get(name);
				PrintStream stdout = System.out;
				System.setOut(new PrintStream( new OutputStream() {public void write( int b ){}}));				approximation = approximateSystem(system, steps, delta, nth);
				System.setOut(stdout);
				for (int n = 0; n < steps + 1; n++) {
					if (n % nth == 0)
						out.print(n
								+ ","
								+ roundToDecimals(system.getIVP().getT() + n
										* delta, roundPlaces) + ",");
					for (int row = 1; row < system.getEquations() + 1; row++) {
						if (n % nth == 0)
							out.print(approximation[n][row - 1] + ",");
					}
					if (n % nth == 0)
						out.println();
				}
				out.close();
			} catch (Exception e) {
				System.out.println("That didn't work: " + e);
			}
		} else
			System.out.println("That didn't work.");
	}
			
	/**
	 * Approx wizard.
	 *
	 * @param system the system
	 */
	private static void approxWizard(DiffyQSystem system) {
		System.out.println(system);
		System.out.print("Look good? Y/N");
		if (!stdin.nextLine().toLowerCase().equals("y")) return;
		int steps = (int) settings[0];
		double delta = (double) settings[1];
		int nth = (int) settings[2];
		boolean good = false;
		while (good == false) {
			System.out.print("Delta/h=" + delta + " good? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				good = true;
			else {
				System.out.print("Delta? ");
				try {
					delta=Double.valueOf(stdin.nextLine());
				} catch (Exception e) {
					System.out.println("That didn't work: " + e);
				}
			}
		}
		good = false;
		while (good == false) {
			System.out.print("Go from T0=" + system.getIVP().getT() + " to " +
				roundToDecimals(system.getIVP().getT() + steps * delta, roundPlaces)
				+ "? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				good = true;
			else {
				System.out.print("New upper bound: ");
				try {
					steps=(int) ((Double.valueOf(stdin.nextLine())-system.getIVP().getT())/delta);
				} catch (Exception e) {
					System.out.println("That didn't work: " + e);
				}
			}
		}
		good = false;
		while (good == false) {
			System.out.print("Display multiples of " + delta*nth + "? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				good = true;
			else {
				System.out.print("New nth: ");
				try {
					nth=(int) (Double.valueOf(stdin.nextLine())/delta);
				} catch (Exception e) {
					System.out.println("That didn't work: " + e);
				}
			}
		}
		settings = new Object[] {steps,delta,nth};
		approximateSystem(system, steps, delta, nth);
	}

	/**
	 * New system.
	 */
	private static void newSystem() {
		DiffyQSystem newSystem = null;
		while (newSystem == null) {
			try {
				System.out.print("System size? ");
				newSystem = new DiffyQSystem(Integer.valueOf(stdin.nextLine()));
			} catch (Exception e) {
				System.out.println("That didn't work: " + e);
				newSystem=null;
			}
		}
		while (newSystem.getName() == null || newSystem.getName() == ""){
			System.out.print("Name it (not something hard to type):");
			String name = stdin.nextLine();
			if (name == "") continue;
			System.out.println(name + " look good? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y")) newSystem.setName(name);
		}
		while (newSystem.getP() == null) {
			String[][] p = new String[newSystem.getEquations()][newSystem
					.getEquations()];
			for (int row = 0; row < newSystem.getEquations(); row++)
				for (int column = 0; column < newSystem.getEquations(); column++) {
					System.out.print("p[" + (row + 1) + "," + (column + 1) + "]: ");
					p[row][column] = stdin.nextLine();
				}
			simpleMatrixPrint(p);
			System.out.println("Look good? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				try {
					newSystem.setP(p);
				} catch (Exception e) {
					System.out.println("No good: " + e);
				}
		}
		while (newSystem.getG() == null) {
			String[] g = new String[newSystem.getEquations()];
			for (int row = 0; row < newSystem.getEquations(); row++) {
				System.out.print("g[" + (row + 1) + "]: ");
				g[row] = stdin.nextLine();
			}
			simpleVectorPrint(g);
			System.out.println("Look good? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				try {
					newSystem.setG(g);
				} catch (Exception e) {
					System.out.println("No good: " + e);
				}
		}
		boolean good = false;
		while (good == false) {
			System.out.print("T0: ");
			try {
				newSystem.getIVP().setT(Double.valueOf(stdin.nextLine()));
			} catch (Exception e) {
				System.out.println("That didn't work: " + e);
			}
			System.out.print("T0=" + newSystem.getIVP().getT() + " good?");
			if (stdin.nextLine().toLowerCase().equals("y"))
				good = true;
		}
		while (newSystem.getIVP().getValues() == null) {
			double[] v = new double[newSystem.getEquations()];
			for (int row = 0; row < newSystem.getEquations(); row++) {
				System.out.print("y(" + newSystem.getIVP().getT() + ")[" + (row
						+ 1) + "]: ");
				try {
					v[row] = Double.valueOf(stdin.nextLine());
				} catch (Exception e) {
					System.out.println("That didn't work: " + e);
				}
			}
			simpleVectorPrint(v);
			System.out.println("Look good? Y/N");
			if (stdin.nextLine().toLowerCase().equals("y"))
				try {
					newSystem.getIVP().setValues(v);
				} catch (Exception e) {
					System.out.println("No good: " + e);
				}
		}
		System.out.println(newSystem);
		System.out.println("Look good? Y/N");
		if (stdin.nextLine().toLowerCase().equals("y"))
			systems.put(newSystem.getName(),newSystem);
		else System.out.println("That's too bad. Time to start over.");
	}

	/**
	 * Simple vector print.
	 *
	 * @param vector the vector
	 */
	private static void simpleVectorPrint(double[] vector) {
		for (double each : vector) System.out.print(each + "\t");
		System.out.println();
	}

	/**
	 * Simple vector print.
	 *
	 * @param vector the vector
	 */
	private static void simpleVectorPrint(String[] vector) {
		for (String each : vector) System.out.print(each + "\t");
		System.out.println();
	}

	/**
	 * Simple matrix print.
	 *
	 * @param matrix the matrix
	 */
	public static void simpleMatrixPrint(double[][] matrix){
		for (double[] eachRow : matrix) {
			for (double each : eachRow) System.out.print(each + "\t");
			System.out.println();
		}
	}
	
	/**
	 * Simple matrix print.
	 *
	 * @param matrix the matrix
	 */
	public static void simpleMatrixPrint(String[][] matrix){
		for (String[] eachRow : matrix) {
			for (String each : eachRow) System.out.print(each + "\t");
			System.out.println();
		}
	}

	/**
	 * Round to decimals.
	 *
	 * @param d the d
	 * @param c the c
	 * @return the double
	 */
	public static double roundToDecimals(double d, int c) {
		int temp = (int) ((d * Math.pow(10, c)));
		return (((double) temp) / Math.pow(10, c));
	}

	/**
	 * Approximate system.
	 *
	 * @param system the system
	 * @param steps the steps
	 * @param delta the delta
	 * @param nth the nth
	 * @return the double[][]
	 */
	public static double[][] approximateSystem(DiffyQSystem system, int steps,
			double delta, int nth) {
		double[][] approximation = null;
		try {
			MathEval matheval = new MathEval();
			approximation = new double[steps + 1][2];
			System.out.print("IVP: t=" + system.getIVP().getT());
			matheval.setVariable("t", system.getIVP().getT());
			for (int i = 1; i < system.getEquations() + 1; i++) {
				System.out.print(" y" + i + "="
						+ system.getIVP().getValues()[i - 1]);
				matheval.setVariable("y" + i, system.getIVP().getValues()[i - 1]);
				approximation[0][i - 1] = system.getIVP().getValues()[i - 1];
			}
			System.out.println(" " + steps + " steps of " + delta
					+ ", printing every " + nth + "th");
			for (int n = 1; n < steps + 1; n++) {
				matheval.setVariable("t",
						roundToDecimals(system.getIVP().getT() + n * delta, roundPlaces));
				for (int row = 1; row < system.getEquations() + 1; row++)
					matheval.setVariable("y" + row, approximation[n - 1][row - 1]);
				if (n % nth == 0)
					System.out
							.print("Step "
									+ n
									+ "/"
									+ steps
									+ ", t="
									+ roundToDecimals(system.getIVP().getT() + n
											* delta, roundPlaces) + ":");
				for (int row = 1; row < system.getEquations() + 1; row++) {
					for (int column = 1; column < system.getEquations() + 1; column++) {
						approximation[n][row - 1] += matheval.evaluate("("
								+ system.getP()[row - 1][column - 1] + ")*y" + column);
					}
					approximation[n][row - 1] = approximation[n - 1][row - 1]
							+ delta
							* (approximation[n][row - 1] + matheval.evaluate(system
									.getG()[row - 1]));
					if (n % nth == 0)
						System.out.print(" " + "y" + row + "="
								+ approximation[n][row - 1]);
				}
				if (n % nth == 0)
					System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return approximation;
	}
}
